set(CMAKE_BUILD_TYPE                  "Debug" CACHE STRING "" FORCE)

set(WITH_ASSERT_ABORT                 ON  CACHE BOOL "" FORCE)
set(WITH_BUILDINFO                    OFF CACHE BOOL "" FORCE)
set(WITH_DOC_MANPAGE                  OFF CACHE BOOL "" FORCE)
set(WITH_GTESTS                       ON  CACHE BOOL "" FORCE)

set(WITH_COMPILER_ASAN                ON CACHE BOOL "" FORCE)
set(WITH_LIBMV_SCHUR_SPECIALIZATIONS  OFF CACHE BOOL "" FORCE)
set(WITH_STRSIZE_DEBUG                ON CACHE BOOL "" FORCE)
set(WITH_PYTHON_SAFETY                ON  CACHE BOOL "" FORCE)

set(WITH_COMPILER_SHORT_FILE_MACRO    OFF CACHE BOOL "" FORCE)
set(WITH_CLANG_TIDY                   OFF CACHE BOOL "" FORCE)
set(WITH_COMPILER_CCACHE              ON CACHE BOOL "" FORCE)
set(WITH_LINKER_MOLD                  ON CACHE BOOL "" FORCE)

set(WITH_LLVM                         ON CACHE BOOL "" FORCE)
set(WITH_GHOST_WAYLAND                OFF CACHE BOOL "" FORCE)

set(CMAKE_EXPORT_COMPILE_COMMANDS     ON CACHE BOOL "" FORCE)
